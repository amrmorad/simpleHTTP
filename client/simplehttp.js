import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './simplehttp.html';

Template.external.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.external.events({
  	'click #fetchButton' : function () {
		$('#fetchButton').attr('disabled','true').val('loading...');
		userName = $('#userName').val();
		Meteor.call('fetchFromService', userName, function(err, respJson) {
			if(err) {
				window.alert("Error: " + err.reason);
				console.log("error occured on receiving data on server. ", err );
			} else {
				console.log("respJson: ", respJson);
				//window.alert(respJson.length + ' tweets received.');
				Session.set("recentTweets",respJson);
			}
			$('#fetchButton').removeAttr('disabled').val('Fetch');
		});
	},
});
