import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.methods({
    fetchFromService: function(userName) {
		var url = "https://api.twitter.com/1.1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name="+userName+"&count=10";
		var result = HTTP.get(url, 
			{
				timeout:30000,
				headers: {
					Authorization : 'OAuth oauth_consumer_key="DortpmGqmhsaVmZ6JDXgj64VX",oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",oauth_signature="",oauth_signature_method="HMAC-SHA1",oauth_timestamp="1318622958",oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",oauth_version="1.0"'
				}
			});
		if(result.statusCode==200) {
			var respJson = JSON.parse(result.content);
			console.log("response received.");
			return respJson;
		} else {
			console.log("Response issue: ", result.statusCode);
			var errorJson = JSON.parse(result.content);
			throw new Meteor.Error(result.statusCode, errorJson.error);
		}
	}
});